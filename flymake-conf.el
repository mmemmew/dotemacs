(require 'flymake)

(setq flymake-fringe-indicator-position 'left-fringe)
(setq flymake-suppress-zero-counters t)
(setq flymake-start-on-flymake-mode t)
(setq flymake-no-changes-timeout nil)
(setq flymake-start-on-save-buffer t)
(setq flymake-proc-compilation-prevents-syntax-check t)
(setq flymake-wrap-around t)

(define-key flymake-mode-map (vector ?\C-c ?! ?s) #'flymake-start)
(define-key flymake-mode-map (vector ?\C-c ?! ?d) #'flymake-show-diagnostics-buffer)
(define-key flymake-mode-map (vector ?\C-c ?! ?n) #'flymake-goto-next-error)
(define-key flymake-mode-map (vector ?\C-c ?! ?p) #'flymake-goto-prev-error)
