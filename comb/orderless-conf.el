;;; -*- lexical-binding: t; -*-

(use-package "orderless" 'orderless
  (setq completion-styles '(substring orderless partial-completion))
  (setq completion-category-defaults nil)
  (setq completion-flex-nospace nil)
  (setq completion-pcm-complete-word-inserts-delimiters t)
  (setq completion-show-help nil)
  (setq completion-ignore-case t)
  (setq read-file-name-completion-ignore-case t)
  (setq read-buffer-completion-ignore-case t)
  (setq resize-mini-windows t)

  (setq orderless-component-separator " +"
	orderless-matching-styles '(orderless-prefixes
				    orderless-literal
				    orderless-regexp
				    orderless-strict-leading-initialism)
	orderless-style-dispatchers '(equal-means-literal-dispatcher
				      comma-means-initial-dispatcher))

  ;; unbind space in the minibuffer
  (define-key minibuffer-local-completion-map (vector 32) nil)

  ;;; dispatchers

;;;###autoload
  (defun equal-means-literal-dispatcher (pattern _index _total)
    "If a pattern begins with an equal sign, then we use literals."
    (cond
     ((= (aref pattern 0) ?=)
      (cons 'orderless-literal (substring pattern 1)))))

;;;###autoload
  (defun comma-means-initial-dispatcher (pattern _index _total)
    "If a pattern begins with a comma, then we use initials to match."
    (cond
     ((= (aref pattern 0) ?,)
      (cons 'orderless-strict-leading-initialism (substring pattern 1))))))


(defvar durand-headlong-entered-minibuffer-p nil
  "Whether or not we have entered minibuffer.

This is used for determining if we shall exit the minibuffer when
there is only one candidate left.")

;;;###autoload
(defun durand-headlong-minibuffer-setup-hook ()
  "The entry for the completion to be headlong.
Simply add this function to `minibuffer-setup-hook'."
  ;; NOTE: When we run this function we first enter minibuffer, so we
  ;; set a variable to its appropriate value.
  (set 'durand-headlong-entered-minibuffer-p nil)
  (add-hook 'post-command-hook 'durand-headlong-post-command-hook t)
  (add-hook 'minibuffer-exit-hook 'durand-headlong-minibuffer-exit-hook))

;;;###autoload
(defun durand-headlong-post-command-hook ()
  "Exit the minibuffer if there is only one candidate left.

In practice, when we first enter minibuffer, there is some
abnormal behaviour, so we only check when we have entered the
minibuffer as usual."
  (let ((comps (completion-all-completions
                (minibuffer-contents)
                minibuffer-completion-table minibuffer-completion-predicate
                (- (point) (minibuffer-prompt-end)))))
    (cond
     ((and durand-headlong-entered-minibuffer-p
           comps
           (not (consp (cdr comps))))
      (minibuffer-force-complete-and-exit))
     (t (set 'durand-headlong-entered-minibuffer-p t)))))

;;;###autoload
(defun durand-headlong-minibuffer-exit-hook ()
  "Remove the hooks we added."
  (remove-hook 'post-command-hook 'durand-headlong-post-command-hook)
  (remove-hook 'minibuffer-setup-hook 'durand-headlong-minibuffer-setup-hook)
  (remove-hook 'minibuffer-exit-hook 'durand-headlong-minibuffer-exit-hook))
