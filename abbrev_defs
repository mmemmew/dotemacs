;;-*-coding: utf-8;-*-
(define-abbrev-table 'emacs-lisp-mode-abbrev-table
  '(
    ("df" "" insert-defun :count 2)
    ("dk" "" insert-define-key :count 2)
   ))

