;;; -*- lexical-binding: t; -*-

(require 'dired)
(require 'dired-x)
(require 'files)
(set 'insert-directory-program "gls")
(set 'dired-listing-switches "-alh --dired --group-directories-first")
(setq dired-dwim-target t)

(require 'dired-aux)

(setq dired-isearch-filenames 'dwim)
(setq dired-create-destination-dirs 'ask)
(setq dired-vc-rename-file t)

(require 'wdired)

(setq wdired-allow-to-change-permissions t)
(setq wdired-create-parent-directories t)

(require 'image-dired)

(setq image-dired-external-viewer "open")
