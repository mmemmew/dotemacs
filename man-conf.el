;;; man-conf.el --- Manual pages configurations      -*- lexical-binding: t; -*-

;; Copyright (C) 2021  李俊緯

;; Author: 李俊緯 <mmemmew@gmail.com>
;; Keywords: unix, help, maint

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Configurations for the Man-mode.

;;; Code:

(declare-function 'durand-member
                  (locate-user-emacs-file "common.el"))

;;;###autoload
(defun durand-Man-kill ()
  "Kill the buffer and the tab."
  (interactive)
  (quit-window t)
  (let* ((tabs (funcall tab-bar-tabs-function))
         (tab-names (mapcar (lambda (tab)
                              (cdr (assq 'name tab)))
                            tabs))
         (foundp (durand-member "man page" tab-names
                                #'string=)))
    (cond
     (foundp
      (tab-bar-close-tab-by-name "man page")))))

(advice-add #'Man-kill :override #'durand-Man-kill)

(provide 'man-conf)
;;; man-conf.el ends here
