;;; center-buffer.el --- Center the buffer -*- lexical-binding: t; -*-

;;; Commentary:

;;; This is some code I extracted from "olivetti" as a lightweight
;;; version of that package to center my buffer, only temporarily,
;;; unfortunately.

;;; Code:

;;;###autoload
(defvar center-buffer-width 80
  "The width of the body.")

;;;###autoload
(defun center-buffer-toggle ()
  "Toggle the centering of the buffer."
  (interactive)
  (cond
   ((car (window-margins (selected-window)))
    (center-buffer-off))
   ((center-buffer-on))))

;;;###autoload
(defun center-buffer-on ()
  "Center the buffer."
  (interactive)
  (let* ((mt (/ (- (window-total-width) 80) 2))
	 (fringes (window-fringes))
	 (lf (/ (car fringes) (float (frame-char-width))))
	 (rf (/ (cadr fringes) (float (frame-char-width))))
	 (lm (max (round (- mt lf)) 0))
	 (rm (max (round (- mt rf)) 0)))
    (set-window-margins (selected-window) lm rm)))

;;;###autoload
(defun center-buffer-off ()
  "Reset buffer."
  (interactive)
  (set-window-margins (selected-window) 0 0))

(provide 'center-bufrfer)
;;; center-bufrfer.el ends here.
