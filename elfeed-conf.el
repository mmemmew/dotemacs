;;; elfeed-conf.el --- My configurations for elfeed  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  李俊緯

;; Author: 李俊緯 <mmemmew@gmail.com>
;; Keywords: comm, hypermedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is my configuration file of elfeed, for viewing RSS/atom
;; feeds.

;;; Code:

(use-package "elfeed" 'elfeed)

(setq elfeed-feeds
      (list (list "https://wiwi.video/feeds/videos.atom?sort=-publishedAt"
                  'wiwi)
            (list "https://wiwikuan.com/index.xml"
                  'wiwi 'blog)
            (list "https://protesilaos.com/codelog.xml"
                  'prot 'code)
            (list "https://protesilaos.com/news.xml"
                  'prot 'news)
            (list "https://protesilaos.com/politics.xml"
                  'prot 'politics)
            (list "https://www.youtube.com/feeds/videos.xml?channel_id=UCcXhhVwCT6_WqjkEniejRJQ"
                  'wintergaten 'youtube)
            (list "https://notrelated.xyz/rss"
                  'lukesmith 'notrelated)
            (list "https://lukesmith.xyz/peertube"
                  'lukesmith 'youtube)
            (list "https://www.youtube.com/feeds/videos.xml?channel_id=UCiWXd0nmBjlKROwzMyPV-Nw"
                  'zhichi 'youtube)))

(define-key elfeed-search-mode-map (vector #x78) 'elfeed-search-exit)

;; I still prefer the defaults.

(setq elfeed-show-entry-switch #'switch-to-buffer)
(setq elfeed-show-entry-delete #'elfeed-kill-buffer)

;;;###autoload
(defun elfeed-display-buffer (buffer)
  "Display BUFFER in the bottom window."
  (display-buffer buffer
                  '(display-buffer-reuse-mode-window display-buffer-at-bottom)))

;;;###autoload
(defun durand-elfeed-kill-buffer ()
  "Kill the buffer and delete its window."
  (kill-buffer (current-buffer))
  (cond
   ((one-window-p))
   ((delete-window (get-buffer-window (current-buffer))))))

;;;###autoload
(defun elfeed-search-exit ()
  "Save the database, then kill all related buffers."
  (interactive)
  (elfeed-db-save)
  (mapc
   (function
    (lambda (buffer)
      (cond ((string-match-p "^\\*elfeed" (buffer-name buffer))
             (kill-buffer buffer)))))
   (buffer-list)))

;;;###autoload
(defun durand-eshell-download-sentinel (process status)
  "Inform the user when the download process is terminated.
PROCESS should have the name \"elfeed download video\".

This informs the user when STATUS is the string \"finished\\n\"."
  (cond ((string-equal (process-name process) "elfeed download video"))
        ((error "The download sentinel is applied to a wrong process: %S"
                process)))
  (cond ((string-equal status "finished\n")
         (message "Downloading videos has finished."))))

;;;###autoload
(defun durand-eshell-download-filter (process output)
  "Display OUTPUT in the buffer associated with PROCESS.
By display it is meant that before inserting the ouput the buffer
will first be erased."
  (let ((buffer (process-buffer process)))
    (cond
     (buffer
      (with-current-buffer buffer
        (erase-buffer)
        (insert
         (replace-regexp-in-string "\\|" "" output)))))))

;;;###autoload
(defun durand-elfeed-download-video ()
  "Download the video in the feed."
  (interactive)
  (let ((default-directory "/Users/durand/Desktop/Centre/Vidéos/")
        link)
    (cond ((derived-mode-p 'elfeed-search-mode)
           (setq link (elfeed-entry-link (elfeed-search-selected :ignore-region))))
          ((derived-mode-p 'elfeed-show-mode)
           (setq link (elfeed-entry-link elfeed-show-entry)))
          ((user-error "Not in an elfeed buffer")))
    (message "Downloading starts.")
    (make-process
     :name "elfeed download video"
     :buffer "*elfeed download*"
     :sentinel #'durand-eshell-download-sentinel
     :filter #'durand-eshell-download-filter
     :command (list
               "youtube-dl" "-f" "bestvideo[height<=720]+bestaudio/best[height<=720]/best"
               link))))

(define-key elfeed-search-mode-map (vector ?w) #'durand-elfeed-download-video)
(define-key elfeed-show-mode-map (vector ?w) #'durand-elfeed-download-video)

(provide 'elfeed-conf)
;;; elfeed-conf.el ends here
