;;; notmuch-conf.el --- My configurations of notmuch  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  李俊緯

;; Author: 李俊緯 <mmemmew@gmail.com>
;; Keywords: comm, games, mail, news, unix

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is my configuration file of notmuch.

;;; Code:

;; The package is not installed in the usual package directory. Rather
;; it is included and installed by the notmuch distribution. So we
;; only have to require it here.

(require 'notmuch)

(setq notmuch-show-empty-saved-searches t)
(setq notmuch-saved-searches
      '((:name "inbox" :query "tag:inbox" :key "i")
        (:name "unread" :query "tag:unread" :key "u")
        (:name "recent" :query "date:one-week" :key "r")
        (:name "sent" :query "tag:sent" :key "e")
        (:name "tan" :query "(from:tan or to:tan) and not tag:flagged" :key "t")
        (:name "ming lun" :query "from:mlhsieh or to:mlhsieh and not tag:flagged" :key "m")
        (:name "drafts" :query "tag:draft" :key "d")
        (:name "all mail" :query "*" :key "a")))

(setq notmuch-show-logo nil)
(setq notmuch-column-control t)
(setq notmuch-hello-auto-refresh t)
(setq notmuch-hello-thousands-separator "")
(setq-default notmuch-search-oldest-first nil)

(setq notmuch-hello-sections
      '(notmuch-hello-insert-header
        notmuch-hello-insert-saved-searches))

(setq notmuch-archive-tags '("-inbox" "+archived"))
(setq notmuch-message-replied-tags '("+replied"))
(setq notmuch-message-forwarded-tags '("+forwarded"))
(setq notmuch-show-mark-read-tags '("-unread"))
(setq notmuch-draft-tags '("+draft"))
(setq notmuch-draft-folder "drafts")
(setq notmuch-draft-save-plaintext 'ask)
(setq notmuch-tagging-keys
      (list
       (list (vector ?a) 'notmuch-archive-tags "Archive (remove from inbox)")
       (list (vector ?d) (list "+del" "-inbox" "-archived" "-unread") "Mark for deletion")
       (list (vector ?f) (list "+flag" "-unread") "Flag as important")
       (list (vector ?r) 'notmuch-show-mark-read-tags "Mark as read")
       (list (vector ?s) (list "+spam" "+del" "-inbox") "Mark as spam")
       (list (vector ?t) (list "+todo" "-unread") "Todo")
       (list (vector ?u) (list "+unread") "Mark as unread")))

(setq notmuch-tag-formats
      '(("unread" (propertize tag 'face 'notmuch-tag-unread))
        ("flag" (propertize tag 'face 'notmuch-tag-flagged))))
(setq notmuch-tag-deleted-formats
      '(("unread" (notmuch-apply-face bare-tag `notmuch-tag-deleted))
        (".*" (notmuch-apply-face tag `notmuch-tag-deleted))))

(setq notmuch-mua-compose-in 'current-window)
(setq notmuch-mua-hidden-headers nil)
(setq notmuch-address-command 'internal)
(setq notmuch-always-prompt-for-sender t)
(setq notmuch-mua-cite-function 'message-cite-original)
(setq notmuch-mua-reply-insert-header-p-function 'notmuch-show-reply-insert-header-p-never)
(setq notmuch-mua-user-agent-function #'notmuch-mua-user-agent-full)
(setq notmuch-maildir-use-notmuch-insert t)
(setq notmuch-crypto-process-mime t)
(setq notmuch-crypto-get-keys-asynchronously t)
(setq notmuch-mua-attachment-regexp
      (concat "\\b\\(attache\?ment\\|attached\\|attach\\|"
              "pi[èe]ce\s+jointe?\\)\\b"))

(setq notmuch-show-relative-dates t)
(setq notmuch-show-all-multipart/alternative-parts nil)
(setq notmuch-show-indent-messages-width 0)
(setq notmuch-show-indent-multipart nil)
(setq notmuch-show-part-button-default-action 'notmuch-show-save-part)
(setq notmuch-show-text/html-blocked-images ".") ; block everything
(setq notmuch-wash-citation-lines-prefix 3)
(setq notmuch-wash-citation-lines-suffix 3)
(setq notmuch-unthreaded-show-out nil)
(setq notmuch-message-headers '("To" "Cc" "Subject" "Date"))
(setq notmuch-message-headers-visible t)



(provide 'notmuch-conf)
;;; notmuch-conf.el ends here
