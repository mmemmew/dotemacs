;;; csv-conf.el --- For dealing with CSV files       -*- lexical-binding: t; -*-

;; Copyright (C) 2021  李俊緯

;; Author: 李俊緯 <mmemmew@gmail.com>
;; Keywords: convenience, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;;;###autoload
(defun calculate-grades ()
  "Return an alist of grades and number of repititions."
  (interactive)
  (let (grouped-ones field-start name grades temp-grades num1 num2 num3 current)
    (with-current-buffer "all names.csv"
      (goto-char (point-min))
      (forward-line 1)
      (while (< (point) (point-max))
        (cond
         ((save-excursion (goto-char (line-end-position))
                          (thing-at-point 'number))
          (re-search-forward "," (line-end-position) t 4)
          (setq field-start (point))
          (re-search-forward "," (line-end-position) t)
          (setq grouped-ones
                (cons
                 (cons
                  (upcase
                   (buffer-substring-no-properties
                    field-start (1- (point))))
                  nil)
                 grouped-ones))))
        (forward-line 1)))
    (with-current-buffer "b24189.csv"
      (goto-char (point-min))
      (forward-line 1)
      (while (< (point) (point-max))
        (re-search-forward "," (line-end-position) t)
        (setq field-start (point))
        (re-search-forward "," (line-end-position) t)
        (setq name (buffer-substring-no-properties
                    field-start (1- (point))))
        (re-search-forward "," (line-end-position) t 3)
        (setq num1 (or (thing-at-point 'number) 0))
        (re-search-forward "," (line-end-position) t)
        (setq num2 (or (thing-at-point 'number) 0))
        (re-search-forward "," (line-end-position) t)
        (setq num3 (or (thing-at-point 'number) 0))
        (setq grades
              (cons
               (list name num1 num2 num3)
               grades))
        (forward-line 1)))
    (with-current-buffer "2021-07-02T1637_成績-代數導論二_(MATH2114).csv"
      (goto-char (point-min))
      (forward-line 1)
      (while (< (point) (point-max))
        (re-search-forward "([^)]*,[^)]*)" (line-end-position) t)
        (re-search-forward "," (line-end-position) t 2)
        (setq field-start (point))
        (re-search-forward "@" (line-end-position) t)
        (setq name (buffer-substring-no-properties
                    field-start (1- (point))))
        (re-search-forward "," (line-end-position) t 2)
        (setq num1 (or (thing-at-point 'number) 0))
        (re-search-forward "," (line-end-position) t)
        (setq num2 (or (thing-at-point 'number) 0))
        (setq temp-grades
              (cons
               (append (list name)
                       (alist-get name grades nil nil #'string=)
                       (list num1 num2))
               temp-grades))
        (forward-line 1))
      (setq grades temp-grades)
      (setq temp-grades nil))
    ;; Now grades has the form (name quizI quizII mid quizIII final)
    ;; quizI is actually useless and should not have been collected.
    (while (consp grades)
      (setq current (car grades))
      (setq grades (cdr grades))
      ;; Three scores:
      ;; 1. final
      ;; 2. (final+mid)/2
      ;; 3. (final/2+mid*3/10+ (sum quizs) /10)
      
      ;; abuse the variable field-start to add extra scores
      (setq name (car current))
      (setq field-start
            (cond
             ((assoc name grouped-ones #'string=)
              (message name)
              7)
             (0)))
      (setq num1 (+ field-start (nth 5 current)))
      (setq num2
            (+ field-start
               (/ (+ (nth 3 current)
                     (nth 5 current))
                  2.0)))
      (setq num3
            (+ field-start
               (+ (/ (nth 5 current) 2.0)
                  (/ (* (nth 3 current) 3)
                     10.0)
                  (/ (+ (nth 2 current)
                        (nth 4 current))
                     2.0))))
      (setq temp-grades
            (cons
             (list (car current)
                   (max num1 num2 num3)
                   num1 num2 num3)
             temp-grades)))
    temp-grades))

(provide 'csv-conf)
;;; csv-conf.el ends here
