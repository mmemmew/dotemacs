;;; backup-conf.el --- My configurations concerning backups -*- lexical-binding: t; -*-

(set 'backup-directory-alist (list (cons "." (expand-file-name "backups" load-file-directory))))

(set 'make-backup-files t)

(set 'backup-by-copying t)

(set 'version-control t)

(set 'delete-old-versions t)

(set 'kept-new-versions 2)

(set 'kept-old-versions 2)

(set 'create-lockfiles nil)
