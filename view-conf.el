;;; view-conf.el --- My configurations to view things -*- lexical-binding: t; -*-

;;; View tmeperatures or timers
;;;###autoload
(defun durand-view-timers-or-temps (&optional arg)
  "View the list of timers or view the CPU temperature info.
If ARG is nil, view the list of timers.
If ARG is '(4), view the information about CPU temperature and
fans speed and some others.
If ARG is '(16), view the battery information."
  (interactive "P")
  (cond
   ((null arg)
    (let (fan-speed
          cpu-die-temperature
          remain
          full-capacity
          fullp
          charging
          cycle
          condition
          connected
          battery-temp)
      (with-temp-buffer
;; NOTE: First fans information
        (insert (funcall
                 (plist-get (car (auth-source-search :host "local-computer"))
                            :secret)))
        (call-process-region
         nil nil "sudo"
         nil t nil "-S" "powermetrics"
         "-i1" "-n1" "-ssmc")
        (goto-char (point-min))
        (search-forward "Fan" nil t)
        (setf fan-speed
              (progn (re-search-forward "[[:digit:]]+" (line-end-position) t)
                     (string-to-number (match-string 0)))
              cpu-die-temperature
              (progn (re-search-forward "temperature: \\([[:digit:]]+\\.[[:digit:]]+\\)" nil t)
                     (string-to-number (match-string 1))))
;; NOTE: Now battery charge information
        (erase-buffer)
        (call-process "system_profiler" nil t nil
                      "SPPowerDataType")
        (goto-char (point-min))
        (re-search-forward "Fully Charged: \\(.+\\)$" nil t)
        (setf fullp (match-string-no-properties 1))
        (re-search-forward "Charging: \\(.+\\)$" nil t)
        (setf charging (match-string-no-properties 1))
        (re-search-forward "Full Charge Capacity (mAh): \\([[:digit:]]+\\)$" nil t)
        (setf full-capacity (string-to-number (match-string-no-properties 1)))
        (re-search-forward "State of Charge (%): \\([[:digit:]]+\\)" nil t)
        (setf remain (string-to-number (match-string-no-properties 1)))
        (re-search-forward "Cycle Count: \\(.+\\)$" nil t)
        (setf cycle (string-to-number (match-string-no-properties 1)))
        (re-search-forward "Condition: \\(.+\\)$" nil t)
        (setf condition (match-string-no-properties 1))
        (re-search-forward "Connected: \\(.+\\)$" nil t)
        (setf connected (match-string-no-properties 1))
;; NOTE: Now battery temperature
        (erase-buffer)
        (call-process "ioreg" nil t nil "-n" "AppleSmartBattery" "-r")
        (goto-char (point-min))
        (re-search-forward "Temperature..=." nil t)
        (re-search-forward "[[:digit:]]+" nil t)
        (setf battery-temp
              (/ (string-to-number (match-string-no-properties 0))
                 100.0)))
      (message
       (concat
        (format "fan: %d, temp: %s, battery temp: %.2f"
                fan-speed cpu-die-temperature battery-temp)
        "\n"
        (format "Full: %d, remaining: %d%s, fullp: %s, charging: %s, connected: %s, cycles: %d, condition: %s"
                full-capacity remain "%%" fullp charging connected cycle condition)))))
   ((equal arg (list 4)) (list-timers))
   (t
    (user-error "Unsupported ARG: %S" arg))))

(define-obsolete-function-alias 'durand-view-timers 'durand-view-timers-or-temps
  "2020-09-02" "View the list of timers.")

;;;###autoload
(defun durand-view-process (&optional arg)
  "View the list of processes"
  (interactive "P")
  (cond
   ((equal arg (list 4))
    (list-processes))
   ((equal arg (list 16))
    (proced))
   ((message "%s" (process-list)))))

;;; My map for viewing

;;;###autoload
(defvar durand-view-map
  ;; Against every good principle I guess...
  (let ((map (list 'keymap "View")))
    (define-key map (vector ?v) #'view-mode)
    (define-key map (vector ?P) #'durand-view-process)
    (define-key map (vector ?t) #'durand-view-timers-or-temps)
    (define-key map (vector ?f) #'durand-focus)
    (define-key map (vector ?e) #'eshell)
    (define-key map (vector ?p) #'durand-chercher-pdf)
    (define-key map (vector ?o) #'olivetti-mode)
    (define-key map (vector ?c) #'calendar)
    (define-key map (vector ?r) #'choose-recent-file)
    (define-key map (vector ?n) #'novel)
    (define-key map (vector ?O) #'durand-maybe-enable-outline)
    (define-key map (vector ?w) #'durand-eww-map)
    map)
  "The keymap that is related to my custom functions about viewing.")
;;; Hyper key

(define-key global-map (vector ?\H-n) #'novel)

;;; About view-mode

(require 'view)

(define-key view-mode-map (vector 108) #'recenter-top-bottom)
(define-key view-mode-map (vector 76) #'reposition-window)

(define-key view-mode-map (vector ?j) #'View-scroll-line-forward)
(define-key view-mode-map (vector ?k) #'View-scroll-line-backward)
(define-key view-mode-map (vector ?\M-n) #'forward-page)
(define-key view-mode-map (vector ?\M-p) #'backward-page)
(define-key global-map (vector 3 118) durand-view-map)

;;; Open things

;;;###autoload
(defvar durand-open-targets
  (list (list "Safari" "open" "-a" "Safari"
              (list "https://www.youtube.com"
                    "https://www.gmail.com"
                    "https://www.facebook.com"
                    "https://protesilaos.com"
                    "https://wmail1.cc.ntu.edu.tw/index.php?url=https%3A%2F%2Fwmail1.cc.ntu.edu.tw%2F"))
        (list "Terminal" "open" "-a" "Terminal" (list
                                                 "/Users/durand/Desktop/Centre/Vidéos/"
                                                 "/Users/durand/Desktop/Centre/Musique/"
                                                 "/Users/durand/Downloads/"))
        (list "Messenger" "open" "-a" "Messenger"))
  "Targets to open by `durand-open-object'.

This is an association list whose elements have `car' equal to
the choice presented to the user, and have `cdr' a list that will
be passed to `make-process'.

See the documentation for `durand-open-object' for the formats of
targets.")

;;;###autoload
(defun durand-target-extra-arg (target)
  "Determine if TARGET has room for extra arguments.
This returns the list of extra argument slots, or nil if there is
none."
  (let (temp result)
    (while (consp target)
      (setq temp (car target))
      (setq target (cdr target))
      (cond
       ;; We shall not test numbers by `eq'.
       ((equal temp -1)
        (setq result (cons -1 result)))
       ((consp temp)
        (setq result (cons temp result)))))
    (nreverse result)))

;;;###autoload
(defun durand-replace-extra-args (target extra-args)
  "Replace slots for extra-args in TARGET by EXTRA-ARGS.
This does not check if the lengths match, though in theory they
should.

And this will delete any `nil' values from the result."
  (let  (temp result)
    (while (consp target)
      (setq temp (car target))
      (setq target (cdr target))
      (cond
       ((or (eq temp -1)
            (consp temp))
        (setq result (cons (car extra-args)
                           result))
        (setq extra-args (cdr extra-args)))
       ((setq result (cons temp result)))))
    (delq nil (nreverse result))))

;;;###autoload
(defun durand-open-object (&optional arg)
  "Open something.
What can be opened is controlled by the variable
`durand-open-targets'.

If ARG is non-nil, and if the chosen target contains -1 in the
command line options, then read a string to replace that -1. If
the chosen target has a sub-list, then use that sub-list as
options to choose from."
  (interactive "P")
  (let* ((targets durand-open-targets)
         (choice (completing-read "Open: " targets nil t))
         (commands (cdr (assoc choice targets #'string=)))
         (extra-arg-slots (durand-target-extra-arg commands))
         (extra-arg (cond
                     (arg
                      (mapcar (lambda (extra-arg-slot)
                                (cond
                                 ((eq extra-arg-slot -1)
                                  (read-string "Extra argument: "))
                                 ((consp extra-arg-slot)
                                  (completing-read
                                   "Extra argument: " extra-arg-slot))))
                              extra-arg-slots)))))
    (setq commands (durand-replace-extra-args commands extra-arg))
    (make-process :name "durand-open"
                  :buffer nil
                  :command commands)))

;;; C-c o => 3 111
(define-key global-map (vector 3 111) #'durand-open-object)

;;; Convert downloaded videos from youtube to the URL again and copy
;;; the result.

;;;###autoload
(defun durand-convert-youtube-video-to-url (video-name)
  "Convert VIDEO-NAME to its original url."
  (interactive (list (read-string "Video name: ")))
  (cond
   ((string-match "\\.[^.]+$" video-name)
    ;; with extension
    (kill-new
     (concat
      "https://www.youtube.com/watch?v="
      (substring-no-properties
       video-name
       (- (match-beginning 0) 11)
       (match-beginning 0)))))
   (t
    ;; no extension
    (kill-new
     (concat "https://www.youtube.com/watch?v="
             (substring-no-properties video-name -11))))))

;;; moving by paragraphs

;;;###autoload
(defun durand-scroll-next-paragraph (&optional arg)
  "Scroll so that the next ARG paragraph is at the top."
  (interactive "p")
  (let ((forward-p (> arg 0))
        pos)
    (save-excursion
      (goto-char (window-start))
      (cond
       (forward-p
        (while (> arg 0)
          (cond
           ((re-search-forward "^\\s-*$" (line-end-position) t)
            (forward-line 1)))
          (while (not (re-search-forward "^\\s-*$" (line-end-position) t))
            (forward-line 1))
          (setq arg (1- arg))))
       (t
        (while (< arg 0)
          (cond
           ((re-search-forward "^\\s-*$" (line-end-position) t)
            (forward-line -1)))
          (while (not (re-search-forward "^\\s-*$" (line-end-position) t))
            (forward-line -1))
          (setq arg (1+ arg)))))
      (setq pos (point)))
    (set-window-start (selected-window) pos)))

;;;###autoload
(defun durand-scroll-prev-paragraph (&optional arg)
  "Scroll so that the previous ARG paragraph is at the top."
  (interactive "p")
  (cond ((numberp arg)) ((setq arg 1)))
  (durand-scroll-next-paragraph (- arg)))

;;; focus mode

;;;###autoload
(defvar durand-focus-map '(keymap "focus")
  "The keymap for the focus mode.")

(define-key durand-focus-map (vector ?\s-v) #'view-mode)
(define-key durand-focus-map (vector ?\s-x) #'durand-focus)
(define-key durand-focus-map (vector ?\M-\s-f) #'durand-focus-focus)
(define-key view-mode-map (vector ?f) #'durand-scroll-next-paragraph)
(define-key view-mode-map (vector ?b) #'durand-scroll-prev-paragraph)

;;;###autoload
(define-minor-mode durand-focus
  "Focus mode." nil " Foc" nil
  (cond
   (durand-focus
    (setq olivetti-body-width 80)
    (olivetti-mode 1)
    (cond (current-prefix-arg (view-mode 1))))
   ((setq olivetti-body-width 80)
    (variable-pitch-mode -1)
    (view-mode -1)
    (olivetti-mode -1)
    (text-scale-increase 0))))

;;;###autoload
(defun durand-focus-focus (&optional arg)
  "Toggle the font size and `variable-pitch-mode'.
ARG will be (prefix-numeric-value prefix-arg) in interactive
uses.

If ARG is non-zero and odd, toggle font-size.

If ARG is non-zero and the second bit is non-zero, toggle
`variable-pitch-mode'."
  (interactive "p")
  (setq arg (prefix-numeric-value arg))
  (cond
   ((= arg 0)
    (cond
     ((and text-scale-mode
           (/= text-scale-mode-amount 0))
      (text-scale-increase 0))
     ((text-scale-increase 2)))
    (variable-pitch-mode 'toggle))
   (t
    (cond
     ((= (% arg 2) 1)
      (cond
       ((and text-scale-mode
             (/= text-scale-mode-amount 0))
        (text-scale-increase 0))
       ((text-scale-increase 2)))))
    (cond
     ((= (logand arg 2) 2)
      (variable-pitch-mode 'toggle))))))
