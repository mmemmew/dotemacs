;;; glsl-conf.el --- My configurations for glsl-mode  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  李俊緯

;; Author: 李俊緯 <mmemmew@gmail.com>
;; Keywords: convenience, extensions, faces, files, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(use-package "glsl" 'glsl)

(setq glsl-other-file-alist
      '(("\\.frag$" (".vert" ".vs"))
        ("\\.vert$" (".frag" ".fs"))
        ("\\.fs$" (".vs" ".vert"))
        ("\\.vs$" (".fs" ".frag"))))

(define-key glsl-mode-map (vector 'S-tab) #'ff-find-other-file)

(provide 'glsl-conf)
;;; glsl-conf.el ends here
