;;; c-conf.el --- configurations for C-mode          -*- lexical-binding: t; -*-

;; Copyright (C) 2021  李俊緯

;; Author: 李俊緯 <durand@MacBook-Pro-de-Severe.local>
;; Keywords: c, convenience, languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; My configurations for editing C files.

;;; Code:

(setq c-macro-preprocessor "gcc -E -C -o - -")

;; We can achieve the functionality of the original binding
;; comment-region by comment-dwim bound to "M-;".
(define-key c-mode-map (vector 3 3) #'compile)
;; This proves to be too convenient not to bind.
(define-key c-mode-map (vector 'S-tab) #'ff-find-other-file)

;; generate pragmas automatically.
(define-key c-mode-map (vector ?ù) #'c-generate-pragma)

;;;###autoload
(defun c-generate-pragma ()
  "Generate default pragmas for a C header file.
If invoked on a C file, include the corresponding header file."
  (interactive)
  (cond
   ((not (derived-mode-p 'c-mode 'c++-mode)))
   ((string-match-p "hp*$"(buffer-name))
    (insert "#ifndef ")
    (insert (replace-regexp-in-string
             "\\." "_"
             (upcase
              (file-name-nondirectory
               (buffer-file-name)))))
    (newline)
    (insert "#define ")
    (insert (replace-regexp-in-string
             "\\." "_"
             (upcase
              (file-name-nondirectory
               (buffer-file-name)))))
    (newline 2)
    (insert "#endif")
    (forward-line -1))
   (t
    (insert (format "#include \"%s\""
                    (replace-regexp-in-string
                     "c\\(p*\\)$" "h\\1" (buffer-name))))
    (newline))))

(provide 'c-conf)
;;; c-conf.el ends here
