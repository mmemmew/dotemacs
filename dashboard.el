;;; dashboard.el --- My daashboard -*- lexical-binding: t; -*-

;;;###autoload
(defvar dashboard-buffer-name "durand"
  "The name of the buffer for the dashboard.")

;;;###autoload
(defvar dashboard-cat-text
  (list
   "                                      "
   "                       /\\             "
   "                      /  \\            "
   "                     /    =\\=         "
   "                    /  -     \\=-----  "
   "                   /  |.|          /  "
   "                 \\\\    -   -      /   "
   "                  \\\\      |.|    |    "
   "                 / \\\\      -    /     "
   "                /   \\\\         /      "
   "   ||==========|     ========\\=|      "
   "   ||==========| --            |      "
   "               | ||     /====  |      "
   "               == |    /     \\  \\     "
   "                  |   |       \\  \\    "
   "                   ====        ====   ")
  "A clumsy text representation of a cute cat.")

;;;###autoload
(defun dashboard (&optional force)
  "Create the dashboard buffer.
If FORCE is non-nil, re-gerenate the dashboard buffer."
  (interactive)
  (let ((dashboard-exists-p (get-buffer dashboard-buffer-name))
        (dashboard (get-buffer-create dashboard-buffer-name))
        (inhibit-read-only t))
    (cond
     ((and (not force) dashboard-exists-p)
      (switch-to-buffer dashboard))
     (t
      (with-current-buffer dashboard
        (erase-buffer)
        (cond
         ((display-graphic-p)
          (let ((image (create-image (expand-file-name "cat.png" user-emacs-directory))))
            (insert
             (make-string 3 10)
             (center-string-in-width
              (propertize " " 'display image 'rear-nonsticky '(display))
              (round (- (window-body-width)
                        (* (car (image-size image)) 1)))))))
         ((mapc (function
                 (lambda (str)
                   (insert
                    (center-string-in-width str (- (window-body-width) 13))
                    "\n")))
                dashboard-cat-text)))
        (newline 5)
        (insert (center-string-in-width "Dashboard" (window-body-width)))
        (read-only-mode 1)
        (set 'mode-line-format '("%e" (:eval (modeline-format-dashboard))))
        (view-mode 1))))
    dashboard))

(set 'initial-buffer-choice #'dashboard)

;;; Modify killing buffers

;;;###autoload
(defun dont-kill-dashboard-fn ()
  "Don't kill the dashboard buffer."
  (cond
   ((eq (current-buffer) (get-buffer dashboard-buffer-name))
    ;; The function message returns the message, so applying not will
    ;; produce nil as needed.
    (not (message "Don't kill the dashboard buffer.")))
   (t)))

(add-hook 'kill-buffer-query-functions 'dont-kill-dashboard-fn)

(provide 'dashboard)
;;; dashboard.el ends here.
