;;; completion-conf.el --- My configurations for completions inside Emacs  -*- lexical-binding: t; -*-

;; Author: JSDurand <mmemmew@gmail.com>
;; Created: 2021-01-22 21:41:30
;; Version: 0.0.1
;; Keywords: matching

;; This file is NOT part of GNU/Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Thsi file is my configurations of the built in completion framework
;; in Emacs. I intend to configure it to use my yet-to-come completion
;; style using suffix trees.

;;; Code:

(require 'minibuffer)
(require 'simple)

(setq completion-styles '(initials substring partial-completion regex flex))
(setq completion-category-defaults nil)
(setq completion-flex-nospace nil)
(setq completion-pcm-complete-word-inserts-delimiters t)
(setq completion-show-help nil)
(setq completion-ignore-case t)
(setq read-file-name-completion-ignore-case t)
(setq read-buffer-completion-ignore-case t)
(setq resize-mini-windows t)

(set 'completions-format 'one-column)

(define-key minibuffer-local-completion-map (vector 'down) #'durand-embark-scroll-down-or-go-to-completions)
(define-key minibuffer-local-completion-map (vector 'up) #'durand-embark-scroll-up-or-go-to-completions)
(define-key minibuffer-local-completion-map (vector ?\s-v) #'durand-completion-toggle-display)
(define-key minibuffer-local-completion-map (vector 32) nil)
(define-key minibuffer-local-must-match-map (vector 32) nil)
(define-key completion-list-mode-map (vector 'down) #'durand-completion-scroll-down-or-go-to-minibuffer)
(define-key completion-list-mode-map (vector 'up) #'durand-completion-scroll-up-or-go-to-minibuffer)

;;;###autoload
(defvar durand-headlong-entered-minibuffer-p nil
  "Whether or not we have entered minibuffer.

This is used for determining if we shall exit the minibuffer when
there is only one candidate left.")

;;;###autoload
(defun durand-headlong-minibuffer-setup-hook ()
  "The entry for the completion to be headlong.
Simply add this function to `minibuffer-setup-hook'."
  ;; NOTE: When we run this function we first enter minibuffer, so we
  ;; set a variable to its appropriate value.
  (set 'durand-headlong-entered-minibuffer-p nil)
  (add-hook 'post-command-hook 'durand-headlong-post-command-hook t)
  (add-hook 'minibuffer-exit-hook 'durand-headlong-minibuffer-exit-hook))

;;;###autoload
(defun durand-headlong-post-command-hook ()
  "Exit the minibuffer if there is only one candidate left.

In practice, when we first enter minibuffer, there is some
abnormal behaviour, so we only check when we have entered the
minibuffer as usual."
  (let ((comps (completion-all-completions
                (minibuffer-contents)
                minibuffer-completion-table minibuffer-completion-predicate
                (- (point) (minibuffer-prompt-end)))))
    (cond
     ((and durand-headlong-entered-minibuffer-p
           comps
           (not (consp (cdr comps))))
      (minibuffer-force-complete-and-exit))
     (t (set 'durand-headlong-entered-minibuffer-p t)))))

;;;###autoload
(defun durand-headlong-minibuffer-exit-hook ()
  "Remove the hooks we added."
  (remove-hook 'post-command-hook 'durand-headlong-post-command-hook)
  (remove-hook 'minibuffer-setup-hook 'durand-headlong-minibuffer-setup-hook)
  (remove-hook 'minibuffer-exit-hook 'durand-headlong-minibuffer-exit-hook))

;; Make sure `minibuffer-message' respects `inhibit-message'.

(defun durand-minibuffer-respect-inhibit-advice (fn &rest args)
  (cond (inhibit-message)
        (t (apply fn args))))

(advice-add 'minibuffer-message :around 'durand-minibuffer-respect-inhibit-advice)

;;;###autoload
(defun durand-completion-display-after-change (&rest args)
  "Call `minibuffer-completion-help' with no input.

\(fn &rest ARGS)"
  (cond
   ((minibufferp)
    (let ((while-no-input-ignore-events '(selection-request)))
      (while-no-input
        (condition-case nil
            (save-excursion
              (goto-char (point-max))
              (save-match-data
                (let ((inhibit-message t))
                  (minibuffer-completion-help))))
          (quit (abort-recursive-edit))))))))

;; (fset
;;  'durand-completion-display-after-change
;;  (make-byte-code
;;   #x080
;;   (unibyte-string 192 32 135)
;;   (vector #'minibuffer-completion-help)
;;   2
;;   "Call `minibuffer-completion-help' with no input.

;; \(fn &rest ARGS)"
;;   nil))

;;;###autoload
(fset
 'durand-completion-toggle-display
 (make-byte-code
  0
  (unibyte-string 192 9 62              ; determine if it is a member
                  131 14 0
                  ;; memq
                  194 193 192 196 35 130 20 0
                  ;; not memq
                  195 193 192 197 196 36
                  135)
  (vector #'durand-completion-display-after-change
          'after-change-functions
          'remove-hook 'add-hook t nil)
  6
  "Toggle to display the list of completions after each input.

\(fn)"
  nil))

;; (defun durand-completion-toggle-display ()
;;   "Toggle to display the list of completions after each input."
;;   (interactive)
;;   (cond
;;    ((memq #'durand-completion-display-after-change 'post-command-hook)
;;     (remove-hook 'post-command-hook #'durand-completion-display-after-change t))
;;    ((add-hook 'post-command-hook #'durand-completion-display-after-change nil t))))

;;;###autoload
(defun regex-try-completion (string table pred point &optional metadata)
  "The function that tries to complete STRING using completion table \
TABLE for the `regex' style.
Only the elements that satisfy the predicate PRED are considered.

POINT is the position of point within STRING.

The return value is nil if there is no completion, and is t if
STRING is the only possible completion. It is a pair

\(NEWSTRING . NEWPOINT)

otherwise, where NEWSTRING is the completed result and NEWPOINT
is the new position for point."
  (let* ((completion-regexp-list (cons string completion-regexp-list))
         (result (try-completion "" table pred)))
    (cond
     ;; For nil values we don't have to return nil explicitly.
     ((null result) nil)
     ((eq result t))
     ((stringp result) (cons result (length result))))))

;;;###autoload
(defun regex-all-completion (string table pred point &optional metadata)
  "List the possible completions of STRING in completion table \
TABLE for the style `regex'.
Only the elements of TABLE that satisfy the predicate PRED are
considered. POINT is the position of point within STRING.

The return value is a list of completions and may contain the
base-size in the last `cdr'."
  (let ((completion-regexp-list (cons string completion-regexp-list)))
    (all-completions "" table pred)))

(let ((style-elements
       (list #'regex-try-completion
             #'regex-all-completion
             "Simple regular expression completion.
This considers the input as the regular expression itself." ))
      (assoc-result (assoc 'regex completion-styles-alist)))
  (cond
   (assoc-result (setcdr assoc-result style-elements))
   ((setq completion-styles-alist
          (cons (cons 'regex style-elements) completion-styles-alist)))))


(provide 'completion-conf)
;;; completion-conf.el ends here
