;;; skeleton-conf.el --- Configurations of skeletons  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  李俊緯

;; Author: 李俊緯 <mmemmew@gmail.com>
;; Keywords: abbrev, convenience, wp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is the file of my configurations of skeletons.

;;; Code:

;;;###autoload
(define-skeleton insert-define-key "Insert a define-key statement."
  "define key"
  "(define-key "
  '(recursive-edit)
  " (vector "
  '(recursive-edit)
  ") #'"
  '(recursive-edit)
  ")")

;;;###autoload
(define-skeleton insert-defun "Insert a defun statement."
  "defun"
  "(defun "
  '(recursive-edit)
  " ("
  '(recursive-edit)
  ")"
  \n
  (cond ((y-or-n-p "interactive?")
          "(interactive)"))
  & ?\n
  > _ ")")

;;;###autoload
(define-skeleton insert-tex "Insert a LaTeX skeleton."
  "LaTeX"
  "\\documentclass[12pt]{article}
\\usepackage[utf8]{inputenc}
\\usepackage[T1]{fontenc}
\\usepackage{graphicx}
\\usepackage{grffile}
\\usepackage{longtable}
\\usepackage{wrapfig}
\\usepackage{rotating}
\\usepackage[normalem]{ulem}
\\usepackage{amsmath}
\\usepackage{mathrsfs}
\\usepackage{textcomp}
\\usepackage{amssymb}
\\usepackage{capt-of}
\\usepackage{hyperref}
\\usepackage{amsfonts, amsthm}
\\usepackage{enumitem}
\\usepackage{tikz}
\\author{"
  (let ((author (read-string "Author: " "Durand")))
    (cond ((string-empty-p author)
           (prog1 "\\vspace{-1ex}"
             (setq v1 "")))
          ((setq v1 author))))
  "}\n"
  "\\date{"
  (let ((date (read-string
               "Date: "
               (format-time-string "%Y--%m--%d"))))
    (cond ((string-empty-p date)
           "\\vspace{-1ex}")
          (date)))
  "}\n"
  "\\title{\\vspace{-5ex}"
  (setq v2 (read-string "Title: ")) "}\n"
  "\\hypersetup{\n"
  "  pdfauthor={" v1 "},\n"
  "  pdftitle={" v2 "},\n"
  "  pdfkeywords={},\n"
  "  pdfsubject={},\n"
  "  pdfcreator={" v1 "},\n"
  "  pdflang={English}}\n\n"
  "\\newtheorem{lem}{Lemma}[section]
\\newtheorem{coro}[lem]{Corollary}
\\newtheorem{thm}{Theorem}[section]
\\newtheorem{prop}{Proposition}[section]
\\newtheorem{ex}{Exercise}[section]
\\theoremstyle{definition}
\\newtheorem*{rmk}{Remark}
\\newtheorem{defn}{Definition}[section]

\\newcommand{\\n}{\\mathbb{N}}
\\newcommand{\\z}{\\mathbb{Z}}
\\newcommand{\\q}{\\mathbb{Q}}
\\renewcommand{\\labelenumi}{\\arabic{enumi}.}
\\renewcommand{\\labelenumii}{(\\alph{enumii})}
\\begin{document}
\\maketitle
\n"
  > _
  "\n\\end{document}")

(require 'autoinsert)

(cond ((assoc 'latex-mode auto-insert-alist)
       (setcdr (assoc 'latex-mode auto-insert-alist)
               #'insert-tex))
      ((add-to-list 'auto-insert-alist
                    (cons 'latex-mode #'insert-tex))))

(provide 'skeleton-conf)
;;; skeleton-conf.el ends here
