
(use-package "/usr/local/share/emacs/site-lisp/mu/mu4e" 'mu4e
  (setq mu4e-maildir-list (list "/Users/durand/mbsync"))
  (setq user-mail-address "mmemmew@gmail.com")
  (setq mu4e-completing-read-function #'completing-read)
  (setq message-confirm-send t)
  (setq mu4e~update-buffer-height 5)

  (setq mu4e-context-policy 'pick-first)
  (setq mu4e-attachment-dir "~/Downloads")
  (setq mu4e-mu-binary "/usr/local/bin/mu")
  
  (setq mu4e-view-use-gnus t)
  (setq mu4e-confirm-quit nil)
  (setq mu4e-get-mail-command "mbsync -a") ; mbsync works a lot better!
  (setq mu4e-change-filenames-when-moving t)
  (setq mu4e-view-show-addresses t)     ; show full addresses!
  (setq mu4e-view-show-images t)
  (setq mu4e-sent-messages-behavior 'delete)
  (setq mu4e-use-fancy-chars t)
  (setq message-send-mail-function 'smtpmail-send-it
        smtpmail-stream-type 'starttls
        smtpmail-default-smtp-server "smtp.gmail.com"
        smtpmail-smtp-server "smtp.gmail.com"
        smtpmail-smtp-service 587)

  (define-key mu4e-main-mode-map (vector ?q) #'bury-buffer)
  (define-key mu4e-main-mode-map (vector ?u) #'mu4e-update-mail-and-index)

  (add-to-list
   'mu4e-bookmarks
   (make-mu4e-bookmark
    :name "week no trash no archive"
    :query "date:7d..now AND NOT maildir:/trash AND NOT maildir:/archive"
    :key ?d))

  (add-to-list
   'mu4e-bookmarks
   (make-mu4e-bookmark
    :name "week starred"
    :query "date:7d..now AND maildir:/suivis"
    :key ?s))

  (setq mu4e-maildir-shortcuts
        '(("/INBOX" . ?i)
          ("/archive" . ?a)
          ("/suivis" . ?s)
          ("/drafts" . ?d)))
  
  (setq mu4e-contexts
        (list
         (make-mu4e-context
          :name "Student"
          :enter-func (lambda () (mu4e-message "Entering Student context"))
          :leave-func (lambda () (mu4e-message "Leaving Student context"))
          ;; we match based on the contact-fields of the message
          :match-func
          (lambda (msg)
            (when msg
              (or
               (mu4e-message-contact-field-matches msg :from "tan\\|mlh\\|hsialc\\|tingyu.lee\\|tingyulee")
               (mu4e-message-contact-field-matches msg :to "tan\\|mlh\\|hsialc\\|tingyu.lee\\|tingyulee"))))
          :vars '((user-mail-address . "mmemmew@gmail.com")
                  (user-full-name . "李俊緯")
                  (mu4e-compose-signature . "生 俊緯")
                  (mu4e-sent-folder . "/gmail/sent")
                  (smtpmail-smtp-user . "mmemmew")
                  (smtpmail-local-domain . "gmail.com")
                  (smtpmail-default-smtp-server . "smtp.gmail.com")
                  (smtpmail-smtp-server . "smtp.gmail.com")
                  (smtpmail-smtp-service . 587)))
         (make-mu4e-context
          :name "NCTS"
          :enter-func (lambda () (mu4e-message "Switch to the NCTS context"))
          :leave-func (lambda () (mu4e-message "Leave NCTS context"))
          ;; no leave-func
          ;; we match based on the maildir of the message
          ;; this matches maildir /Arkham and its sub-directories
          :match-func (lambda (msg)
                        (when msg
                          (or
                           (mu4e-message-contact-field-matches msg :to "chunweilee@ncts.ntu.edu.tw")
                           (mu4e-message-contact-field-matches msg :from "chunweilee@ncts.ntu.edu.tw"))))
          :vars '((user-mail-address . "chunweilee@ncts.ntu.edu.tw")
                  (user-full-name . "李俊緯")
                  (mu4e-compose-signature .
                                          (concat
                                           "Sincerely Yours,\n"
                                           "俊緯"))))
         (make-mu4e-context
          :name "Durand"
          :enter-func (lambda () (mu4e-message "Entering Durand context"))
          :leave-func (lambda () (mu4e-message "Leaving Durand context"))
          ;; we match based on the contact-fields of the message
          :match-func (lambda (msg)
                        (when msg
                          (or
                           (mu4e-message-contact-field-matches msg :to "mmemmew@gmail.com")
                           (mu4e-message-contact-field-matches msg :from "mmemmew@gmail.com"))))
          :vars '((user-mail-address . "mmemmew@gmail.com")
                  (user-full-name . "Durand")
                  (mu4e-compose-signature . "Sévère Durand")
                  (mu4e-sent-folder . "/gmail/sent")
                  (smtpmail-smtp-user . "mmemmew")
                  (smtpmail-local-domain . "gmail.com")
                  (smtpmail-default-smtp-server . "smtp.gmail.com")
                  (smtpmail-smtp-server . "smtp.gmail.com")
                  (smtpmail-smtp-service . 587))))))
