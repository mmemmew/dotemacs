;;; tex-conf.el --- My configurations of TeX         -*- lexical-binding: t; -*-

;; Copyright (C) 2021  李俊緯

;; Author: 李俊緯 <mmemmew@gmail.com>
;; Keywords: tex

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is my configuration file for (La)TeX. In particular I use and
;; configure the package AUCTeX here.

;;; Code:

(add-to-list 'load-path "/usr/local/share/emacs/site-lisp/")
(load "auctex.el" nil t t)
(load "preview-latex.el" nil t t)

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq TeX-view-program-selection
      '((output-dvi "open")
        (output-pdf "PDF Tools")
        (output-html "open")))

;;; I don't want to split the screen.

;;;###autoload
(defun durand-TeX-pdf-tools-sync-view-a ()
  "Advice to `TeX-pdf-tools-sync-view', which won't pop buffers."
  (unless (fboundp 'pdf-tools-install)
    (error "PDF Tools are not available"))
  (unless TeX-PDF-mode
    (error "PDF Tools only work with PDF output"))
  (add-hook 'pdf-sync-backward-redirect-functions
            #'TeX-source-correlate-handle-TeX-region)
  (if (and TeX-source-correlate-mode
           (fboundp 'pdf-sync-forward-search))
      (with-current-buffer (or (when TeX-current-process-region-p
                                 (get-file-buffer (TeX-region-file t)))
                               (current-buffer))
        (pdf-sync-forward-search))
    (let ((pdf (TeX-active-master (TeX-output-extension))))
      (switch-to-buffer (or (find-buffer-visiting pdf)
                            (find-file-noselect pdf))))))

(add-hook 'TeX-after-compilation-finished-functions
          #'TeX-revert-document-buffer)

(setq pdf-sync-forward-display-action
      (list (list 'display-buffer-same-window)))

(setq pdf-sync-backward-display-action
      (list (list 'display-buffer-same-window)))

(advice-add #'TeX-pdf-tools-sync-view :override
            #'durand-TeX-pdf-tools-sync-view-a)

;;; Correlate

(load "/usr/local/share/emacs/site-lisp/auctex/tex.el" nil t)

(setq TeX-source-correlate-start-server t)
(TeX-source-correlate-mode 1)

;;; Section display

(load "/usr/local/share/emacs/site-lisp/auctex/font-latex.el" nil t)

(setq font-latex-fontify-sectioning 1.3)

;;; Add a font macro command

(load "/usr/local/share/emacs/site-lisp/auctex/latex.el" nil t)

(add-to-list 'LaTeX-font-list
             '(?\C-k "" "" "\\mathfrak{" "}"))

;;; Dollars

(setq TeX-electric-math (cons "\\(" "\\)"))

(define-key LaTeX-mode-map (vector ?\§) #'durand-insert-escape)

;;;###autoload
(defun durand-insert-escape ()
  "Insert an escape character."
  (interactive)
  (insert "\\"))

;;; Parens don't require space

(add-hook 'LaTeX-mode-hook #'durand-prepare-latex)

;;; Brackets handling

(mapc
 (function
  (lambda (cell)
    (define-key LaTeX-mode-map (vector (car cell)) (cdr cell))))
 (list
  (cons ?\( #'open-paren)
  (cons ?{ #'open-curly)
  (cons ?\[ #'open-bracket)
  (cons 'backspace #'durand-delete-pair)
  (cons ?\) #'end-exit-paren)
  (cons ?à #'open-back-paren)))

;;;###autoload
(defun open-paren ()
  "open parenthesis inserts a matching pair"
  (interactive)
  (progn
    (insert "()")
    (backward-char)))

;;;###autoload
(defun open-curly ()
  "open curly inserts a matching pair"
  (interactive)
  (progn
    (insert "{}")
    (backward-char)))

;;;###autoload
(defun open-bracket ()
  "open bracket inserts a matching pair"
  (interactive)
  (progn
    (insert "[]")
    (backward-char)))

;;;###autoload
(defun durand-delete-pair ()
  "Delete the matching pair"
  (interactive)
  (cond ((region-active-p) ; if the region is active, then do the original thing
         (delete-backward-char 1))
        ((looking-back "\\(\\\\(\\|\\\\\\[\\)" (- (point) 2))
         (save-excursion
           (backward-char 1)
           (ignore-errors
             (forward-sexp 1)
             (delete-char -2)))
         (delete-char -2))
        ((memq (char-before) '(?\( ?\[ ?\{))
         (save-excursion
           (backward-char 1)
           (ignore-errors
             (forward-sexp 1)
             (delete-char -1)))
         (delete-char -1))
        (t
         (backward-delete-char-untabify 1))))

;;;###autoload
(defun end-exit-paren ()
  "Use closing pasenthesis to exit the parenthesis"
  (interactive)
  (let ((ch (char-after nil))
        (ch-list '(?\) ?\} ?\] ?\$)))
    (cond ((memq ch ch-list) (forward-char))
          ((looking-at-p "\\(\\\\)\\|\\\\]\\|\\\\}\\)")
           (forward-char 2))
          (t (self-insert-command 1)))))

;;;###autoload
(defun open-back-paren ()
  "Use \"à\" to go back to the parenthesis."
  (interactive)
  (let ((ch (char-before nil))
        (ch-list '(?\) ?\} ?\] ?\$)))
    (cond ((looking-back "\\(\\\\)\\|\\\\]\\|\\\\}\\)" (- (point) 2))
           (forward-char -2))
          ((memq ch ch-list) (backward-char))
          (t (self-insert-command 1)))))

(make-variable-buffer-local 'parens-require-spaces)

;;;###autoload
(defun durand-prepare-latex ()
  "Prepare some settings in LateX mode."
  (modify-syntax-entry ?\\ "_")
  (turn-on-auto-fill)
  (setq parens-require-spaces nil))

;;; Don't insert a new line between the section and the label.

;;;###autoload
(defun durand-latex-delete-newline ()
  "If the character before point is a newline, then delete it."
  (cond
   ((= (char-before) ?\n)
    (delete-char -1))))

(setq LaTeX-section-hook
      '(LaTeX-section-heading LaTeX-section-title
        LaTeX-section-section durand-latex-delete-newline
        LaTeX-section-label))

;;; Expansions

(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

(define-key LaTeX-math-mode-map (LaTeX-math-abbrev-prefix) t)

(set-default 'LaTeX-math-abbrev-prefix "ù")

(define-key LaTeX-math-mode-map
  (LaTeX-math-abbrev-prefix) LaTeX-math-keymap)

(cond ((boundp 'LaTeX-math-keymap))
      ((defvar LaTeX-math-keymap)))

(define-key LaTeX-math-keymap [?$] 'durand-insert-display-equation)
(define-key LaTeX-math-keymap [?o] 'durand-insert-o-things)

(setq LaTeX-math-list '((?$ durand-insert-display-equation)
                        (?o durand-insert-o-things)))

;;;###autoload
(defun durand-insert-display-equation ()
  "Insert display equation symbols."
  (interactive)
  (insert "\\[\\]")
  (goto-char (- (point) 2)))

;;;###autoload
(defvar durand-o-things-list nil
  "A list of things associated with the \"o\" key.
Each entry should have the form (key . macro),
where the macro part is without the backslash.")

(setq durand-o-things-list
      (list
       (cons "x" "otimes")
       (cons "s" "sum")
       (cons "p" "prod")
       (cons "+" "oplus")
       (cons "o" "circ")
       (list "{" "{" "}")))

;;;###autoload
(defun durand-insert-o-things ()
  "Insert a symbol associated with \"o\" key.
The list is in the variable `durand-o-things-list'"
  (interactive)
  (let ((thing (minibuffer-with-setup-hook 'durand-headlong-minibuffer-setup-hook
                 (completing-read
                  "Chois une chose associée à \"o\":" (mapcar 'car durand-o-things-list)
                  nil t "^"))))
    (let ((associated (alist-get thing durand-o-things-list nil nil #'string=)))
      (cond
       ((not (consp associated))
        ;; length = 1
        (insert (format "\\%s" (assoc-default thing durand-o-things-list #'string=))))
       ((null (cddr associated))
        ;; length = 2
        (insert (format "\\left\\%s" (car associated)))
        (save-excursion
          (insert
           (format "\\right\\%s" (cadr associated)))))
       (t
        (user-error "Weird associated: %S" associated))))))

;; Automatic braces

(setq TeX-electric-sub-and-superscript t)

;;; If I need to type nested documents frequently, the following will
;;; come in handy.

;; (setq-default TeX-master nil)

(provide 'tex-conf)
;;; tex-conf.el ends here
