;;; search-conf.el --- My configurations for searching -*- lexical-binding: t; -*-

(setq search-whitespace-regexp ".*?")
(setq isearch-lax-whitespace t)
(setq isearch-lazy-count t)
(setq isearch-allow-scroll 'unlimited)

;;;###autoload
(defun durand-search-replace-symbol (&optional arg)
  "Search the symbol at point and query-replace.

If ARG is negative, replace backwards."
  (interactive "p")
  (isearch-forward-symbol-at-point arg)
  (isearch-repeat-backward 1)
  (isearch-exit)
  (isearch-query-replace-regexp (cond ((< arg 0) arg))))

(define-key global-map (vector ?\M-s ?%) #'durand-search-replace-symbol)





(provide 'search-conf)

;;; search-conf.el ends here
