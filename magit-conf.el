;; Well, a lot of dependencies

(use-package "dash.el" 'dash)
(use-package "with-editor" 'with-editor)
(use-package "transient/lisp" 'transient)
(use-package "magit/lisp/" 'magit)
