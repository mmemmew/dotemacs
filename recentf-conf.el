;;; recentf-conf.el --- My configurations concerning recentf -*- lexical-binding: t; -*-

(require 'recentf)

(set 'recentf-save-file (expand-file-name "recentf" load-file-directory))

(recentf-mode 1)

;;;###autoload
(defun choose-recent-file ()
  "Choose a recently visited file to visit.
This uses `completing-read' to choose a file from `recentf-list'."
  (interactive)
  (let ((choice (completing-read "Choose a recently visited file: "
                                 recentf-list 'file-exists-p t)))
    (find-file choice)))
