;;; org-journal-conf.el --- Configurations of the journal  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  李俊緯

;; Author: 李俊緯 <mmemmew@gmail.com>
;; Keywords: files, languages, outlines

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Configurations for org-journal

;;; Code:

(use-package "org-journal" 'org-journal)

(setq org-journal-dir (expand-file-name "journal" org-directory))

(setq org-journal-file-type 'monthly)


(provide 'org-journal-conf)
;;; org-journal-conf.el ends here
