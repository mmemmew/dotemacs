;;; outline-conf.el --- Configurations for outline   -*- lexical-binding: t; -*-

;; Copyright (C) 2021  李俊緯

;; Author: 李俊緯 <mmemmew@gmail.com>
;; Keywords: convenience, files, languages, maint, matching, outlines, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; My configurations for `outline-minor-mode'.

;;; Code:

(require 'outline)

;;;###autoload
(defvar durand-outline-regexp-alist nil
  "An alist that associates regular expressions with major modes
that enable `outline-minor-mode'.")

(setq durand-outline-regexp-alist
      (list (cons 'emacs-lisp-mode
                  (rx-to-string
                   '(seq bol (>= 3 ";") (syntax ?-)) t))))

;;;###autoload
(defvar durand-outline-blocklist (list 'org-mode 'outline-mode)
  "The list of major modes in which one should not use
`outline-minor-mode'.")

;;;###autoload
(defun durand-maybe-enable-outline ()
  "Enable `outline-minor-mode' if appropriate."
  (interactive)
  (cond
   ((apply #'derived-mode-p durand-outline-blocklist)
    (user-error "Should not use `outline-minor-mode' with %S"
                major-mode))
   ((null outline-minor-mode)
    (let ((regexp (cdr-safe (assq major-mode durand-outline-regexp-alist))))
      (cond
       (regexp
        (setq-local outline-regexp regexp))))
    (outline-minor-mode 1)
    (message "`outline-minor-mode' enabled"))
   (t
    (outline-minor-mode -1)
    (message "`outline-minor-mode' disabled"))))

(setq outline-minor-mode-highlight 'override)
(setq outline-minor-mode-cycle t)

(define-key outline-minor-mode-map (vector 3 ?\C-n) #'outline-next-visible-heading)
(define-key outline-minor-mode-map (vector 3 ?\C-p) #'outline-previous-visible-heading)
(define-key outline-minor-mode-map (vector 3 ?\C-f) #'outline-forward-same-level)
(define-key outline-minor-mode-map (vector 3 2) #'outline-backward-same-level)
(define-key outline-minor-mode-map (vector 3 1) #'outline-show-all)
(define-key outline-minor-mode-map (vector 3 ?\C-o) #'outline-hide-other)
(define-key outline-minor-mode-map (vector 3 ?\C-u) #'outline-up-heading)


(provide 'outline-conf)
;;; outline-conf.el ends here
