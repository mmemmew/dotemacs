;;; rime-conf.el --- Configurations of rime -*- lexical-binding: t; -*-

;;; Author: Durand
;;; Version: 0.0.1

;;; Commentary:

;;; My configurations of rime.

;;; Code:

(use-package "dash.el" 'dash)

(use-package "emacs-rime" 'rime
  (define-key global-map (vector ?\s-i) #'durand-set-input-method)
  (define-key minibuffer-local-map (vector ?\s-i) #'durand-set-input-method)
  (setq rime-librime-root "~/elisp_packages/librime/dist")
  (add-to-list 'rime-translate-keybindings "RET")
  (add-to-list 'rime-translate-keybindings "[")
  (add-to-list 'rime-translate-keybindings "]")
  (setq default-input-method "rime"
        ;; rime-show-candidate 'posframe
        rime-user-data-dir (expand-file-name "rime" load-file-directory))

;;;###autoload
  (defun durand-set-input-method ()
    "Set the input method to \"rime\" or nil."
    (interactive)
    (cond
     ((string= current-input-method "rime") (set-input-method nil))
     ((set-input-method "rime"))))
  
;;; HACK: Fix a function
  (defun durand-rime-send-keybinding-a ()
    "Fix a weird function."
    (let* ((parsed (rime--parse-key-event last-input-event))
           (key-raw (car parsed))
           (key (if (numberp key-raw)
                    key-raw
                  (pcase key-raw
                    ('home 65360)
                    ('left 65361)
                    ('up 65362)
                    ('right 65363)
                    ('down 65364)
                    ('prior 65365)
                    ('next 65366)
                    ('delete 65535)
                    ('return 10)
                    (_ key-raw))))
           (mask (cdr parsed)))
      (unless (numberp key)
        (error "Can't send this keybinding to librime"))
      (rime-lib-process-key key mask)
      (rime--redisplay)
      (rime--refresh-mode-state)))
  
  (advice-add 'rime-send-keybinding :override #'durand-rime-send-keybinding-a))

(provide 'rime-conf)
;; rime-conf.el ends here
