;;; theme

(setq modus-themes-syntax '(yellow-comments))
(setq modus-themes-bold-constructs t)
(setq modus-themes-slanted-constructs t)
(setq modus-themes-mode-line '(3d borderless padded))
(setq modus-themes-hl-line '(intense accented))
(setq modus-themes-org-blocks 'gray-background)

(setq modus-themes-subtle-line-numbers t)
(setq modus-themes-region nil)
(setq modus-themes-links '(background bold))
                                        ; Try it out
(setq modus-themes-headings '((t . rainbow)))

(setq modus-themes-org-agenda
      '((header-block . (variable-pitch scale-title))
        (header-date . (gray-scale))
        (scheduled . rainbow)
        ()))

(setq modus-themes-scale-headings t)
(setq modus-themes-scale-1 1.05)
(setq modus-themes-scale-2 1.1)
(setq modus-themes-scale-3 1.15)
(setq modus-themes-scale-4 1.2)
(setq modus-themes-scale-5 1.3)

(setq modus-themes-variable-pitch-ui nil)
(setq modus-themes-variable-pitch-headings t)

(use-package "protesilaos/modus-themes" 'modus-vivendi-theme
  (load-theme 'modus-vivendi t))

;;; Archives

;; (setq modus-vivendi-theme-slanted-constructs t
;;       modus-vivendi-theme-bold-constructs t
;;       ;; modus-vivendi-theme-3d-modeline t
;;       modus-vivendi-theme-mode-line '3d
;;       modus-vivendi-theme-diffs nil
;;       modus-vivendi-theme-completions nil
;;       modus-vivendi-theme-org-blocks 'rainbow
;;       modus-vivendi-theme-rainbow-org-src-blocks t
;;       modus-vivendi-theme-variable-pitch-headings t
;;       modus-vivendi-theme-headings '((t . rainbow))
;;       ;; modus-vivendi-theme-rainbow-headings t
;;       ;; modus-vivendi-theme-section-headings t
;;       modus-vivendi-theme-scale-headings t
;;       modus-vivendi-theme-scale-1 1.05
;;       modus-vivendi-theme-scale-2 1.1
;;       modus-vivendi-theme-scale-3 1.15
;;       modus-vivendi-theme-scale-4 1.2
;;       modus-vivendi-theme-scale-5 1.3)

;; (setq modus-operandi-theme-slanted-constructs t
;;       modus-operandi-theme-bold-constructs t
;;       ;; modus-operandi-theme-3d-modeline t
;;       modus-operandi-theme-mode-line '3d
;;       modus-operandi-theme-diffs nil
;;       modus-operandi-theme-completions nil
;;       modus-operandi-theme-org-blocks 'rainbow
;;       modus-operandi-theme-rainbow-org-src-blocks t
;;       modus-operandi-theme-variable-pitch-headings t
;;       modus-operandi-theme-headings '((t . rainbow))
;;       ;; modus-operandi-theme-rainbow-headings t
;;       ;; modus-operandi-theme-section-headings t
;;       modus-operandi-theme-scale-headings t
;;       modus-operandi-theme-scale-1 1.05
;;       modus-operandi-theme-scale-2 1.1
;;       modus-operandi-theme-scale-3 1.15
;;       modus-operandi-theme-scale-4 1.2
;;       modus-operandi-theme-scale-5 1.3)
