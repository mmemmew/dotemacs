(use-package "embark" 'embark
  (define-key global-map (vector ?\s-,) #'embark-act)
  (define-key global-map (vector ?\s-.) #'embark-act-noexit)

  (define-key minibuffer-local-completion-map (vector ?\M-q) #'embark-collect-toggle-view)
  (define-key minibuffer-local-completion-map (vector ?\s-:) #'embark-collect-live)

  (define-key minibuffer-local-completion-map (vector ?\M-o) #'embark-export)
  (define-key minibuffer-local-completion-map (vector ?\C-o) #'embark-export)
  (define-key minibuffer-local-completion-map (vector ?\M-v) #'embark-switch-to-collect-completions)
  (define-key embark-collect-mode-map (vector ?,) #'embark-act)
  (define-key embark-collect-mode-map (vector ?\M-o) #'embark-export)
  (define-key embark-collect-mode-map (vector ?\C-o) #'embark-export)
  (define-key embark-collect-mode-map (vector ?\M-t) #'toggle-truncate-lines)
  (define-key embark-collect-mode-map (vector ?\M-q) #'embark-collect-toggle-view)
  (define-key embark-collect-mode-map (vector 'down) #'durand-completion-scroll-down-or-go-to-minibuffer)
  (define-key embark-collect-mode-map (vector 'up) #'durand-completion-scroll-up-or-go-to-minibuffer)
  
  (define-key minibuffer-local-completion-map (vector 'down) #'durand-embark-scroll-down-or-go-to-completions)
  (define-key minibuffer-local-completion-map (vector 'up) #'durand-embark-scroll-up-or-go-to-completions)
  (define-key completion-list-mode-map (vector 'down) #'durand-completion-scroll-down-or-go-to-minibuffer)
  (define-key completion-list-mode-map (vector 'up) #'durand-completion-scroll-up-or-go-to-minibuffer)

  ;; (define-key embark-file-map (vector ?v) #'view-file)

  (define-key embark-general-map (vector ?v) (lambda (&rest _args)
                                               (interactive)
                                               (view-mode)
                                               ;; (cond
                                               ;;  ((buffer-file-name)
                                               ;;   (view-file (buffer-file-name))))
                                               ))
  (define-key embark-general-map (vector 127) #'durand-switch-other-buffer)

;;;###autoload
  (defun durand-switch-other-buffer (&rest _args)
    "switch to the last used buffer"
    (interactive)
    (switch-to-buffer (other-buffer)))

;;;###autoload
  (defun prot-embark--live-collect-fit-window (&rest _)
    "Fit Embark's live collect window to its buffer.
To be added to `embark-collect-post-revert-hook'."
    (when (string-match-p "Live" (buffer-name))
      (fit-window-to-buffer (get-buffer-window)
                            (floor (frame-height) 2) 1)))
  
;;;###autoload
  (defun prot-embark--live-buffer-p ()
    "Determine presence of a linked live collect buffer."
    (let* ((buf-link embark-collect-linked-buffer)
           (buf-name (buffer-name buf-link)))
      (when buf-name
        (string-match-p "Embark Live Collect" buf-name))))

;;;###autoload
  (defun prot-embark-live-collect-toggle ()
    "Toggle `embark-live-collect', call `prot-embark-live-collect-hook'."
    (interactive)
    (if (prot-embark--live-buffer-p)
        (kill-buffer embark-collect-linked-buffer)
      (embark-collect-live)))

;;;###autoload
  (defun durand-embark-scroll-down-or-go-to-completions (&optional arg)
    "Scroll down; if this is not feasible, go to the completions buffer.
ARG means do this command ARG times."
    (interactive "p")
    (let ((original-point (point))
          (left (abs (forward-line arg)))
          (completion-windows
           (delq nil
                 (mapcar
                  (lambda (w)
                    (and (consp
                          (delq nil
                                (mapcar (lambda (name)
                                          (string-match-p
                                           name
                                           (buffer-name (window-buffer w))))
                                        durand-completion-buffer-names)))
                         w))
                  (window-list nil 'nomini))))
          (in-completion-p (consp
                           (delq nil
                                 (mapcar
                                  (lambda (name)
                                    (string-match-p name (buffer-name)))
                                  durand-completion-buffer-names)))))
      (cond ((= (point) (point-max))
             (setq left (1+ left)))
            ((get-text-property (point) 'field)
             (goto-char original-point)))
      (cond ((and (> left 0)
                  (consp completion-windows)
                  (not in-completion-p))
             (select-window (car completion-windows))
             (cond
              ((> arg 0)
               (goto-char (point-min))
               (cond
                ((derived-mode-p 'completion-list-mode)
                 (next-completion 1))))
              ((< arg 0)
               (goto-char (point-max))
               (cond
                ((derived-mode-p 'completion-list-mode)
                 (next-completion -1)))))))))

;;;###autoload
  (defun durand-embark-scroll-up-or-go-to-completions (&optional arg)
    "Scroll up; if this is not feasible, go to the completions buffer.
ARG means do this command ARG times."
    (interactive "p")
    (durand-embark-scroll-down-or-go-to-completions (- arg)))

;;;###autoload
  (defun durand-completion-scroll-down-or-go-to-minibuffer (&optional arg)
    "Scroll down; if this is not feasible, go to the mini-buffer.
ARG means do this command ARG times."
    (interactive "p")
    (let ((original-point (point))
          (left (abs (forward-line arg)))
          (minibuffer-active-p (active-minibuffer-window))
          (in-minibuffer-p (minibuffer-window-active-p (selected-window))))
      (cond ((= (point) (point-max))
             (setq left (1+ left)))
            ((get-text-property (point) 'field)
             (goto-char original-point)))
      (cond ((and (> left 0)
                  minibuffer-active-p
                  (not in-minibuffer-p))
             (select-window (active-minibuffer-window))
             (cond
              ((> arg 0)
               (goto-char (point-min)))
              ((< arg 0)
               (goto-char (point-max))))))))

;;;###autoload
  (defun durand-completion-scroll-up-or-go-to-minibuffer (&optional arg)
    "Scroll up; if this is not feasible, go to the mini-buffer.
ARG means do this command ARG times."
    (interactive "p")
    (durand-completion-scroll-down-or-go-to-minibuffer (- arg)))

  ;; (set 'embark-occur-minibuffer-completion t)

  (add-hook 'embark-collect-post-revert-hook #'prot-embark--live-collect-fit-window))

;;; Actually there is no difference between putting the configuration
;;; after and inside the use-package form.

(define-key input-decode-map (vector 27) nil)
(define-key input-decode-map (vector 27) (vector 'escape))

(define-key global-map (vector 'escape) #'embark-act)

(use-package "marginalia" 'marginalia
  (setq marginalia-annotators
        '(marginalia-annotators-heavy
          marginalia-annotators-light))
  (marginalia-mode 1))


