(use-package "pdf-tools/lisp" 'pdf-view)

;; I want to integrate with the global register list.
(setq pdf-view-use-dedicated-register nil)

;; NOTE: Fix a bug

;;;###autoload
(defun durand-pdf-view-position-to-register (register)
  "Store current PDF position in register REGISTER.

See also `point-to-register'.

Modifed by Durand 2021-07-27 10:42:02.828266."
  (interactive
   (list (pdf-view-with-register-alist
           (register-read-with-preview "Position to register: "))))
  (let ((p (assq register (cond (pdf-view-use-dedicated-register
                                 pdf-view-register-alist)
                                (register-alist)))))
    (cond
     (p (setcdr p (pdf-view-registerv-make)))
     (pdf-view-use-dedicated-register
      (setq pdf-view-register-alist
            (cons (cons register (pdf-view-registerv-make))
                  pdf-view-register-alist)))
     ((setq register-alist
            (cons (cons register (pdf-view-registerv-make))
                  register-alist))))))

(advice-add 'pdf-view-position-to-register :override
            'durand-pdf-view-position-to-register)

;;;###autoload
(defun pdf-view-down-half-page-or-previous-page ()
  "Scroll down half page or go to the previous page."
  (interactive)
  (pdf-view-scroll-down-or-previous-page
   (floor (window-body-height) 2)))

;;;###autoload
(defun pdf-view-up-half-page-or-next-page ()
  "Scroll down half page or go to the previous page."
  (interactive)
  (pdf-view-scroll-up-or-next-page
   (floor (window-body-height) 2)))

;; If this information is shown in the mode line, then it will take a
;; century to display a PDF page, probably because it uses such
;; functions as `image-next-line', thus I bind a key to show this
;; information when I want to acquire this information.

;;;###autoload
(defun pdf-view-page-position ()
  (interactive)
  "Show the current position in the page."
  (let* ((edges (pdf-util-image-displayed-edges nil t))
         (percent-floats (pdf-util-scale-pixel-to-relative
                          (cons (car edges) (cadr edges)) nil t))
         (bottom-p
          (cond ((= (window-vscroll nil pdf-view-have-image-mode-pixel-vscroll)
                    (image-next-line 1)))
                ;; The return value of `image-previous-line' is not
                ;; specified in its documentation string, but if
                ;; `image-previous-line' returns a non-nil value, then
                ;; this will return nil; if it returns nil, then this
                ;; still returns nil.
                ((image-previous-line 1) nil)))
         (str
          (cond
           ((and bottom-p (= (cdr percent-floats) 0)) "All")
           ((= (cdr percent-floats) 0) "Top")
           (bottom-p "Bot")
           ((format "%d%%%%" (* 100 (cdr percent-floats)))))))
    (message str)))

(define-key pdf-view-mode-map (vector ?j) #'pdf-view-next-line-or-next-page)
(define-key pdf-view-mode-map (vector ?k) #'pdf-view-previous-line-or-previous-page)
(define-key pdf-view-mode-map (vector ?y) #'pdf-view-previous-line-or-previous-page)
(define-key pdf-view-mode-map (vector ?d) #'pdf-view-up-half-page-or-next-page)
(define-key pdf-view-mode-map (vector ?u) #'pdf-view-down-half-page-or-previous-page)
(define-key pdf-view-mode-map (vector 'tab) #'pdf-view-page-position)
(define-key pdf-view-mode-map (vector ?\C-<) #'image-scroll-left)
(define-key pdf-view-mode-map (vector ?\C->) #'image-scroll-right)

(use-package "pdf-tools/lisp" 'pdf-history)
(use-package "pdf-tools/lisp" 'pdf-links)
(use-package "pdf-tools/lisp" 'pdf-outline)
(use-package "pdf-tools/lisp" 'pdf-sync)
(use-package "pdf-tools/lisp" 'pdf-occur)
(use-package "pdf-tools/lisp" 'pdf-annot)
(use-package "pdf-tools/lisp" 'pdf-tools)
(pdf-tools-install)

